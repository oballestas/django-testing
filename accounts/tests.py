from django.test import TestCase, TransactionTestCase
from django.db.utils import IntegrityError
from django.utils.text import slugify

from .models import User

# Create your tests here.

"""
* Probar que el email sea unico. [OK]
* Probar que el atributo `email` [OK], del modelo de usuario sea obligatorio.
* Probar `first_name`, `last_name` del modelo de usuario sea obligatorio.
* Se debe autogenerar un `slug` con el primer y segundo nombre de usuario. [OK]
"""


class TestUser(TransactionTestCase):

    # Activity

    def test_first_name_mandaroty(self):
        user = User(
            username='oballestas',
            password='123456',
            email='oballestas25@gmail.com',
            last_name='Ballestas'
        )

        with self.assertRaises(IntegrityError):
            user.save()
        user.first_name = 'Oscar'
        user.save()

    def test_last_name_mandaroty(self):
        user = User(
            username='oballestas',
            password='123456',
            email='oballestas25@gmail.com',
            first_name='Oscar'
        )

        with self.assertRaises(IntegrityError):
            user.save()
        user.last_name = 'Ballestas'
        user.save()

    def test_slug_autogeneration(self):
        """ """
        user = User(
            username='oballestas',
            password='123456',
            email='oballestas25@gmail.com',
            first_name='Oscar',
            last_name='Ballestas',
        )
        expected_slug = slugify(user.get_full_name())
        user.save()
        db_user = User.objects.get(email=user.email)
        self.assertEqual(db_user.slug, expected_slug)

    def test_user_email_unique(self):
        """"""
        user = User(
            username='oballestas',
            password='123456',
            first_name='Oscar',
            last_name='Ballestas',
            email='oballestas25@gmail.com'
        )
        user.save()
        user_two = User(
            username='oballestas2',
            password='123456',
            first_name='Oscar2',
            last_name='Ballestas2',
            email='oballestas25@gmail.com'
        )
        with self.assertRaisesMessage(IntegrityError, expected_message='UNIQUE constraint failed: accounts_user.email'):
            user_two.save()

    def test_user_email_mandatory(self):
        """
        Testing `accounts.User` model has the `email` column as not-null
        """
        user = User(
            username='oballestas',
            password='123456',
            first_name='Oscar',
            last_name='Ballestas'
        )
        with self.assertRaises(IntegrityError):
            user.save()
        bad_values = [
            '',
            '    ',
            '@masfaf',
            'jaime@',
            'mimosa',
        ]
        for bad_val in bad_values:
            user.email = bad_val
            with self.assertRaises(IntegrityError, msg=f"Invalid validation for value {bad_val}"):
                user.save()  # Guardando el modelo.

        user.email = 'mimosa@gmail.com'
        user.save()
        db_user = User.objects.get(email=user.email)
        self.assertEqual(user.id, db_user.id)
